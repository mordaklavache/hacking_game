// #name,parentName,isDirectory,content

var end =
[
    ["/", null, true, null],
    ["note.txt", "/", false, "October 2nd, <br/>After the tragic death of our fellow friend Artie Mitchell, we decided today to give his best creation his name as a legacy. This artificial intelligence system was your baby, and we couldn't make it without you. You were a genius and we all want you to be remembered. Aaron."]
]
